import React from 'react';
import { IonList, IonCard, IonCardContent, IonCardHeader, IonIcon, IonGrid, IonRow, IonCol} from '@ionic/react';
import { download } from 'ionicons/icons';
import { BookFileData } from '../data/BookFileData';

type Props = {
    items: BookFileData[],
    onItemClicked: any,
}

export class ClickableBookFileList extends React.Component<Props> {
    render(): React.ReactNode {
        return (
            <IonList>
                {this.props.items.map((item, index) => <IonCard
                    key={index}
                    onClick={() => this.props.onItemClicked(item.url, item.fileName)}
                    style={{'userSelect': 'none'}}>
                        <IonCardHeader>
                            <b>
                                {item.fileName}
                            </b>
                        </IonCardHeader>
                        <IonCardContent>
                            <IonGrid>
                                <IonRow>
                                    <IonCol>
                                        Server path: {item.url} <br/>
                                    </IonCol>
                                    <IonCol>
                                        <IonIcon icon={download} style={{'fontSize': 64}}/>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonCardContent>
                </IonCard>)}
            </IonList>
        )
    }
}