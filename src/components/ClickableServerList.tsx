import React from 'react';
import { IonList, IonCard, IonCardContent, IonCardHeader} from '@ionic/react';
import { ServerData } from '../data/ServerData';

type Props = {
    items: ServerData[],
    onItemClicked: any,
    onLoaded: any,
}

export class ClickableServerList extends React.Component<Props> {

    componentDidMount() {
        this.props.onLoaded(this.props.items.map(item => item.url));
    }

    render(): React.ReactNode {
        return (
            <IonList>
                {this.props.items.map((item, index) => <IonCard
                    key={index}
                    onClick={() => this.props.onItemClicked(item.url)}
                    style={{'userSelect': 'none'}}>
                        <IonCardHeader>
                            {item.url}
                        </IonCardHeader>
                        <IonCardContent>
                        </IonCardContent>
                </IonCard>)}
            </IonList>
        )
    }
}