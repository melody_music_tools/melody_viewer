import React from 'react';
import { IonList, IonCard, IonCardContent, IonCardHeader} from '@ionic/react';
import { TuneData } from '../data/TuneData';

export class ClickableTuneList extends React.Component<{items: TuneData[], itemClickedCallback: any}> {
    render(): React.ReactNode {
        return (
            <IonList>
                {this.props.items.map((item, index) => <IonCard key={index} onClick={() => this.props.itemClickedCallback(item.internalIndex)} style={{'userSelect': 'none'}}>
                    <IonCardHeader>
                        <b>{item.number}</b> {item.name}
                    </IonCardHeader>
                    <IonCardContent>
                        Tune name: {item.tuneName}
                        <br/>
                        Author: {item.author}
                        <br/>
                        Lyricist: {item.lyricist}
                    </IonCardContent>
                </IonCard>)}
            </IonList>
        )
    }
}