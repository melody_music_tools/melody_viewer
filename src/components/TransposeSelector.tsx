import React from 'react';
import { IonSelect, IonSelectOption } from '@ionic/react';

export class TransposeSelector extends React.Component<{value: number, valueChangedCallback: any}> {
    render(): React.ReactNode {
        return (
            <IonSelect value={this.props.value} onIonChange={this.props.valueChangedCallback}>
                <IonSelectOption value={11}>+11</IonSelectOption>
                <IonSelectOption value={10}>+10</IonSelectOption>
                <IonSelectOption value={9}>+9</IonSelectOption>
                <IonSelectOption value={8}>+8</IonSelectOption>
                <IonSelectOption value={7}>+7</IonSelectOption>
                <IonSelectOption value={6}>+6</IonSelectOption>
                <IonSelectOption value={5}>+5</IonSelectOption>
                <IonSelectOption value={4}>+4</IonSelectOption>
                <IonSelectOption value={3}>+3</IonSelectOption>
                <IonSelectOption value={2}>+2</IonSelectOption>
                <IonSelectOption value={1}>+1</IonSelectOption>
                <IonSelectOption value={0}>0</IonSelectOption>
                <IonSelectOption value={-1}>-1</IonSelectOption>
                <IonSelectOption value={-2}>-2</IonSelectOption>
                <IonSelectOption value={-3}>-3</IonSelectOption>
                <IonSelectOption value={-4}>-4</IonSelectOption>
                <IonSelectOption value={-5}>-5</IonSelectOption>
                <IonSelectOption value={-6}>-6</IonSelectOption>
                <IonSelectOption value={-7}>-7</IonSelectOption>
                <IonSelectOption value={-8}>-8</IonSelectOption>
                <IonSelectOption value={-9}>-9</IonSelectOption>
                <IonSelectOption value={-10}>-10</IonSelectOption>
                <IonSelectOption value={-11}>-11</IonSelectOption>
            </IonSelect>
        )
    }
}