import React from 'react';
import { IonSelect, IonSelectOption, IonItem, IonButton, IonIcon } from '@ionic/react';
import { add, remove } from 'ionicons/icons';

type State = {
    value: number,
}

export class ZoomSelector extends React.Component<{value: number, onChanged: any}, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            value: this.props.value,
        }
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    increment() {
        if(this.state.value < 150) {
            this.setState({value: this.state.value + 5});
        }
    }

    decrement() {
        if(this.state.value > 25) {
            this.setState({value: this.state.value - 5});
        }
    }

    changeValue(value: any) {
        this.setState({value: value.detail.value});
        this.props.onChanged(value);
    }

    render(): React.ReactNode {
        return (
            <IonItem lines='none' class='ion-no-padding'>
                <IonButton slot='start' onClick={this.decrement} fill="clear">
                    <IonIcon icon={remove} slot="icon-only"/>
                </IonButton>
                <IonSelect value={this.state.value} onIonChange={this.changeValue}>
                    {
                        Array.from(Array(26).keys())
                            .map(value => (value * 5) + 25)
                            .map(value => <IonSelectOption key={value} value={value}>{value}%</IonSelectOption>)
                    }
                </IonSelect>
                <IonButton slot='end' onClick={this.increment} fill="clear">
                    <IonIcon icon={add} slot="icon-only"/>
                </IonButton>
            </IonItem>
        )
    }
}