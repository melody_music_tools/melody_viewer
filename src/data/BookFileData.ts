export type BookFileData = {
    url: string,
    fileName: string,
}