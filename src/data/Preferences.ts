import { Directory, Filesystem, Encoding } from '@capacitor/filesystem';

type PreferenceData = {
    servers: string[],
}

export class Preferences {
    private preferenceData: PreferenceData;
    private path: string;

    private constructor(preferenceData: PreferenceData, path: string) {
        this.preferenceData = preferenceData;
        this.path = path;
    }

    static async FromFile(path: string): Promise<Preferences> {
        if (path.length < 1) {
            throw new Error('Cannot create file with name of length of 0');
        }

        if (path === '/' || path === '\\') {
            throw new Error('Cannot create config file with invalid name: ' + path);
        }

        let configFolder;
        let configFileName;
        if (path.includes('/')) {
            configFolder = path.substring(0, path.lastIndexOf('/') + 1);
            configFileName = path.split('/').last();
        } else {
            configFolder = '';
            configFileName = path;
        }
        let directoryContents = await Filesystem.readdir({
            directory: Directory.Data,
            path: configFolder
        });
        if (!directoryContents.files.map(file => file.name).contains(configFileName)) {
            let preferences = new Preferences(
                {
                    servers: [],
                },
                path
            );

            await preferences.writeToFilesystem();

            return preferences;
        } else {
            let configFileContent = await Filesystem.readFile({
                directory: Directory.Data,
                path,
                encoding: Encoding.UTF8
            });
            //await Filesystem.writeFile({directory: Directory.Data, path, data: '{}'});
            console.log(configFileContent.data);
            let parsedConfigFileContent: PreferenceData = JSON.parse(configFileContent.data);
            console.log(parsedConfigFileContent);
            return new Preferences(parsedConfigFileContent, path);
        }
    }

    private async delete() {
        await Filesystem.writeFile({directory: Directory.Data, path: this.path, data: '{}'});
    }

    async writeToFilesystem() {
        let data = JSON.stringify(this.preferenceData);
        await Filesystem.writeFile({
            directory: Directory.Data,
            path: this.path,
            data,
            encoding: Encoding.UTF8
        });
    }

    async addServer(serverUrl: string) {
        //console.log(this.preferenceData);
        if(this.preferenceData.servers === undefined) {
            this.preferenceData.servers = [];
        }
        this.preferenceData.servers.push(serverUrl);
        await this.writeToFilesystem();
    }

    async removeServer(serverUrl: string) {
        this.preferenceData.servers.splice(this.preferenceData.servers.indexOf(serverUrl), 1);
        //this.cleanServers();
        await this.writeToFilesystem();
    }

    /*cleanServers() {
        for(let index = 0; index < this.preferenceData.servers.length; index++) {
            if (this.preferenceData.servers[index] === null ||
                this.preferenceData.servers[index] === undefined) {
                    this.preferenceData.servers.splice(index, 1);
            }
        }
    }*/

    getServers(): string[] {
        if (this.preferenceData.servers === undefined) {
            return [];
        }
        //console.log(this.preferenceData)
        //console.log(this.preferenceData.servers)
        return this.preferenceData.servers;
    }
}