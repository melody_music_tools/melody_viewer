export type TuneData = {
    fileName: string,
    number: number,
    name: string,
    tuneName: string,
    author: string,
    lyricist: string,
    internalIndex: number,
}