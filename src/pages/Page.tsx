import {
    IonAccordion,
    IonAccordionGroup,
    IonSearchbar,
    IonItem,
    IonLabel,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonModal,
    IonPage,
    IonToolbar,
    IonInput,
    IonGrid,
    IonRow,
    IonIcon,
    isPlatform,
} from '@ionic/react';
import * as React from 'react';
import { TransposeCalculator, OpenSheetMusicDisplay, Label, EngravingRules } from 'opensheetmusicdisplay';
//import { FilePicker } from '@capawesome/capacitor-file-picker';
import './Page.css';
import { Directory, Filesystem, Encoding } from '@capacitor/filesystem';
import * as Zip from '@zip.js/zip.js';
import { chevronBack, chevronForward, close, book, musicalNotes, swapVertical, folderOpen, folder, trash} from 'ionicons/icons';
import { Http, HttpDownloadFileResult } from '@capacitor-community/http';
import { ClickableTuneList } from '../components/ClickableTuneList';
//import { platform, type } from 'os';
import { TuneData } from '../data/TuneData';
import { TransposeSelector } from '../components/TransposeSelector';
import { ZoomSelector } from '../components/ZoomSelector';
import { Preferences } from '../data/Preferences';
import { ClickableServerList } from '../components/ClickableServerList';
//import { url } from 'inspector';
import { ServerData } from '../data/ServerData';
import { ClickableBookFileList } from '../components/ClickableBookFileList';
import { BookFileData } from '../data/BookFileData';

type State = {
    bookSelectionModalOpen: boolean,
    tuneSelectionModalOpen: boolean,
    serverEditModalOpen: boolean,
    filteredTunes: TuneData[],
    transposeAmount: number,
    preferenceDataServers: ServerData[],
    booksAvailableFromServers: BookFileData[],
}

class Page extends React.Component<{}, State> {

    zoom = 1.0;
    transposeAmount = 0;
    osmdRenderer: OpenSheetMusicDisplay | undefined = undefined;
    currentMusicIndex = 0;
    bookConfig: {file_names: string[], tunes: string[]} | undefined = undefined;
    bookStrings: { [name: string]: string } = {};
    hymnNumberSelectorValue = 0;
    currentServer = '';
    preferences: Preferences | undefined = undefined;
    urlInputValue: string | null | undefined = '';
    selectedServer = '';

    constructor(props: any) {
        super(props);
        this.state = {
            bookSelectionModalOpen: false,
            tuneSelectionModalOpen: false,
            serverEditModalOpen: false,
            filteredTunes: [],
            transposeAmount: 0,
            preferenceDataServers: [],
            booksAvailableFromServers: [],
        }
        this.setHymnSelectorValue = this.setHymnSelectorValue.bind(this);
        this.setZoom = this.setZoom.bind(this);
        this.setTransposeAmount = this.setTransposeAmount.bind(this);
        this.openMusicBookFile = this.openMusicBookFile.bind(this);
        this.renderMusic = this.renderMusic.bind(this);
        this.loadConfigFromFilesystem = this.loadConfigFromFilesystem.bind(this);
        this.previousTune = this.previousTune.bind(this);
        this.nextTune = this.nextTune.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.getPreferences = this.getPreferences.bind(this);
        this.getServerDataList = this.getServerDataList.bind(this);
        this.getBookListForServers = this.getBookListForServers.bind(this);
        this.removeServerFromList = this.removeServerFromList.bind(this);
        this.downloadAndStoreMelodyFile = this.downloadAndStoreMelodyFile.bind(this);

        Filesystem.requestPermissions();
        this.getPreferences();
    }

    async getPreferences(): Promise<Preferences> {
        if (this.preferences === undefined) {
            this.preferences = await Preferences.FromFile('preferences.json');
        }
        //this.preferences.addServer("http://obelia:33333");
        this.setState({preferenceDataServers: this.preferences.getServers().map(serverName => { return {url: serverName} })});
        return this.preferences;
    }

    previousTune() {
        if(this.currentMusicIndex === 0) {
            this.currentMusicIndex = this.bookConfig!.file_names.length - 1;
        } else {
            this.currentMusicIndex--;
        }
        this.renderMusic(this.currentMusicIndex);
    }

    nextTune() {
        if(this.currentMusicIndex === this.bookConfig!.file_names.length - 1) {
            this.currentMusicIndex = 0;
        } else {
            this.currentMusicIndex++;
        }
        this.renderMusic(this.currentMusicIndex);
    }

    setHymnSelectorValue(value: any) {
        let tunes: string[] = this.bookConfig!.file_names;
        let localFilteredTunes = [];
        let searchTerm: string = value.target.value;
        let lowerCaseSearchTerm = searchTerm.toLowerCase();
        for(var i = 0; i < tunes.length; i++)  {
            let fileName = tunes[i];
            if (fileName.includes(lowerCaseSearchTerm)) {
                let number = parseInt(fileName.split('_')[0]);
                let doubleUnderscoreSplit = fileName.split('__');
                let name = doubleUnderscoreSplit.length > 1 ? doubleUnderscoreSplit[0].substring(5).replaceAll('_', ' ') : '';
                let tuneName = doubleUnderscoreSplit.length > 1 ? doubleUnderscoreSplit[1].replaceAll('_', ' ') : '';
                let author = '';
                let lyricist = ''
                localFilteredTunes.push({fileName, number, name, tuneName, author, lyricist, internalIndex: i});
            }
        }
        this.setState({filteredTunes: localFilteredTunes});
    }

    setTransposeAmount(value: any) {
        this.transposeAmount = parseInt(value.detail.value);
        this.renderMusic(this.currentMusicIndex);
    }

    setZoom(value: any) {
        this.zoom = value.detail.value / 100;
        this.renderMusic(this.currentMusicIndex);
    }

    async downloadAndStoreMelodyFile(baseUrl: string, fileName: string) {
        let directory = Directory.Data;
        let encoding = Encoding.UTF8;
        let url = baseUrl + (baseUrl.endsWith('/') ? 'download/' : '/download/') + fileName;

        console.log('GETting with URL: ' + url);

        const fileDownloadResponse: HttpDownloadFileResult = await Http.downloadFile({
            url,
            filePath: fileName,
            fileDirectory: directory,
            method: "GET",
        });

        let zipReader: Zip.ZipReader<unknown>;
        if (isPlatform("desktop")) {
            zipReader = new Zip.ZipReader(new Zip.BlobReader(fileDownloadResponse.blob!));
        } else {
            let file = await Filesystem.readFile({path: fileName, directory: directory});
            zipReader = new Zip.ZipReader(new Zip.Data64URIReader(file.data));
        }

        let entries = await zipReader.getEntries();
 
        // Ensure that directory to save to exists
        let folderContents = await Filesystem.readdir({directory: directory, path: ''});
        if (!folderContents.files.map(file => file.name).contains('musicxml')) {
            Filesystem.mkdir({directory: directory, path: 'musicxml'});
        }

        await Promise.all(entries.map(async entry => {
            return entry.getData?.(new Zip.TextWriter()).then(async rawData => {
                if (entry.filename === "book.json") {
                    return Filesystem.writeFile({directory: directory, path: 'book.json', data: rawData, encoding: encoding});
                } else if (entry.filename.startsWith('musicxml/') && entry.filename.endsWith('.musicxml')) {
                    return Filesystem.writeFile({directory: directory, path: entry.filename, data: rawData, encoding: encoding});
                }
            });
        }));

        if ((await Filesystem.readdir({directory, path: ''})).files.map(file => file.name).contains(fileName)) {
            await Filesystem.deleteFile({directory, path: fileName});
        }
    }

    async openMusicBookFile() {

        /*this.setState({
            bookSelectionModalOpen: true,
        });*/

        //let files = await FilePicker.pickFiles({types: ['*/*'], readData: true});
        //let data = files.files[0].data!;

        await this.loadConfigFromFilesystem();
        this.currentMusicIndex = 0;
        this.renderMusic(0);
    }

    async getMusicXMLString(index: number) {
        let directory = Directory.Data;
        let encoding = Encoding.UTF8;
        let name = this.bookConfig!.file_names[index];

        if(!(name in this.bookStrings)) {
            let filePath = 'musicxml/' + name + '.musicxml';
            await Filesystem.readFile({directory: directory, path: filePath, encoding: encoding}).then((fileContent) => {
                this.bookStrings[name] = fileContent.data;
            });
        }
        return this.bookStrings[name];
    }

    async loadConfigFromFilesystem() {
        let directory = Directory.Data;

        let indexFile = await Filesystem.readFile({path: 'book.json', directory: directory, encoding: Encoding.UTF8});
        this.bookConfig = JSON.parse(indexFile.data);
    }

    async renderMusic(index: number) {
        this.currentMusicIndex = index;
        if (this.bookConfig === undefined) {
            console.log('Book config is undefined, not rendering!');
            return;
        }

        this.osmdRenderer!.load(await this.getMusicXMLString(index)).then(() => {
            this.osmdRenderer!.Sheet.Transpose = this.transposeAmount;
            this.osmdRenderer!.updateGraphic();
            this.osmdRenderer!.zoom = this.zoom;
            console.log('Rendering at transpose: ' + this.osmdRenderer!.Sheet.Transpose);
            this.osmdRenderer!.render();
        });
    }

    closeModal() {
        this.setState({
            bookSelectionModalOpen: false,
        });
    }

    closeHymnSelectionPopover() {
        this.setState({
            bookSelectionModalOpen: false,
        })
    }
    
    componentDidMount() {
        if (this.osmdRenderer !== undefined) {
            return;
        }
        this.osmdRenderer = new OpenSheetMusicDisplay(document.getElementById("osdm")!);
        this.osmdRenderer.setOptions({
            backend: "svg",
            drawTitle: true,
            drawMeasureNumbersOnlyAtSystemStart: true,
            spacingFactorSoftmax: 100,
            drawSubtitle: true,
            //drawingParameters: "compacttight" // don't display title, composer etc., smaller margins
        });
        this.osmdRenderer!.EngravingRules.PickupMeasureWidthMultiplier = 2;
        this.osmdRenderer!.TransposeCalculator = new TransposeCalculator();
        try {
            this.loadConfigFromFilesystem().then(() => {
                this.currentMusicIndex = 0;
                this.renderMusic(0);
            })
        } catch(_e) {
        }
    }

    async getServerDataList(): Promise<ServerData[]> {
        return this.getPreferences().then(preferences => {
            console.log(preferences.getServers());
            return preferences.getServers().map(url => {
                return { url }
            })
        });
    }

    async getBookListForServers(serverBaseURLs: string[]) {
        serverBaseURLs.forEach(serverBaseURL => {
            console.log("getting book list from base: " + serverBaseURL);
            var url = serverBaseURL.endsWith('/') ?
                serverBaseURL + 'books' :
                serverBaseURL + '/books';
            console.log("getting book list from: " + url);
            Http.get({ url }).then(response => {
                let newList = this.state.booksAvailableFromServers;
                newList = newList.concat(response.data.book_names.map((fileName: string) => {
                    return { url: serverBaseURL, fileName }
                }));
                newList = Array.from(new Set(newList));
                this.setState({booksAvailableFromServers: newList.sort()});
                console.log(this.state.booksAvailableFromServers);
            });
        });
    }

    async addServerToList(server: any) {
        let preferences = await this.getPreferences();
        preferences.addServer(server);
        this.setState({preferenceDataServers: preferences.getServers().map(serverName => { return {url: serverName} })});
    }

    async removeServerFromList(server: string) {
        console.log("removing server");
        let preferences = await this.getPreferences();
        preferences.removeServer(server);
        this.setState({
            preferenceDataServers: preferences.getServers().map(serverName => { return {url: serverName} }),
            serverEditModalOpen: false,
        });
    }

    render() {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonButton id='selectMusicBookButton' slot='start' onClick={() => this.setState({bookSelectionModalOpen: true})}>
                                <IonIcon icon={book} slot="icon-only"/>
                            </IonButton>
                            <IonButton id='selectMusicButton' slot='end' onClick={() => this.setState({tuneSelectionModalOpen: true})}>
                                <IonIcon icon={musicalNotes} slot="icon-only"/>
                            </IonButton>
                            <IonButton onClick={this.previousTune}>
                                <IonIcon icon={chevronBack} slot="icon-only"/>
                            </IonButton>
                            <IonButton onClick={this.nextTune}>
                                <IonIcon icon={chevronForward} slot="icon-only"/>
                            </IonButton>
                        </IonButtons>
                        <IonItem lines='none' class='ion-no-padding'>
                            <IonItem lines='none' class='ion-no-padding'>
                                <ZoomSelector value={100} onChanged={this.setZoom}/>
                            </IonItem>
                            <IonItem lines='none' class='ion-no-padding'>
                                <IonIcon icon={swapVertical}/>
                                <TransposeSelector value={this.state.transposeAmount} valueChangedCallback={this.setTransposeAmount}/>
                            </IonItem>
                        </IonItem>
                    </IonToolbar>
                </IonHeader>

                <IonContent fullscreen>
                    <div id="osdm"/> 
                </IonContent>
                <IonModal isOpen={this.state.bookSelectionModalOpen} onDidDismiss={() => this.setState({bookSelectionModalOpen: false})}>
                    <IonAccordionGroup>
                        <IonAccordion>
                            <IonItem slot='header'>
                                Servers
                            </IonItem>
                            <IonItem slot='content'>
                                <IonGrid>
                                    <IonRow>
                                        <IonInput
                                            inputmode='url'
                                            placeholder='e.g. https://c3t3.dev:33333/'
                                            onIonChange={(value) => this.urlInputValue = value.detail.value}/>
                                        <IonButton onClick={() => this.addServerToList(this.urlInputValue)}>
                                            Add
                                        </IonButton>
                                    </IonRow>
                                    <IonRow>
                                        <ClickableServerList
                                            items={this.state.preferenceDataServers}
                                            onItemClicked={(value: string) => {this.selectedServer = value; this.setState({serverEditModalOpen: true});}}
                                            onLoaded={(urls: string[]) => this.getBookListForServers(urls)}/>
                                    </IonRow>
                                </IonGrid>
                            </IonItem>
                        </IonAccordion>
                        <IonAccordion>
                            <IonItem slot='header'>
                                Installed
                            </IonItem>
                            <IonItem slot='content'>
                            </IonItem>
                        </IonAccordion>
                        <IonAccordion>
                            <IonItem slot='header'>
                                Not installed
                            </IonItem>
                            <IonItem slot='content'>
                                <ClickableBookFileList
                                    items={this.state.booksAvailableFromServers}
                                    onItemClicked={this.downloadAndStoreMelodyFile}/>
                            </IonItem>
                        </IonAccordion>
                    </IonAccordionGroup>
                    <IonButton onClick={this.closeModal}>
                        <IonIcon icon={close}/>
                        Close
                    </IonButton>
                    <IonButton>
                        <IonIcon icon={folderOpen}/>
                        Load from file
                    </IonButton>
                </IonModal>
                <IonModal isOpen={this.state.tuneSelectionModalOpen} onDidDismiss={() => this.setState({tuneSelectionModalOpen: false, filteredTunes: []})}>
                    <IonContent>
                        <IonGrid>
                            <IonRow>
                                <IonSearchbar placeholder='by number or name' onIonInput={this.setHymnSelectorValue}/>
                            </IonRow>
                            <IonRow>
                                <ClickableTuneList items={this.state.filteredTunes} itemClickedCallback={(index: number) => {this.setState({tuneSelectionModalOpen: false}); this.renderMusic(index)}}/>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </IonModal>
                <IonModal isOpen={this.state.serverEditModalOpen}>
                    <IonContent>
                        <IonLabel>
                            Edit server
                        </IonLabel>
                        <IonLabel>
                            {this.selectedServer}
                        </IonLabel>
                        <IonButton onClick={() => this.removeServerFromList(this.selectedServer)}>
                            <IonIcon icon={trash}/>
                            Remove Server
                        </IonButton>
                        <IonButton onClick={() => this.setState({serverEditModalOpen: false})}>
                            <IonIcon icon={close}/>
                            Close
                        </IonButton>
                    </IonContent>
                </IonModal>
            </IonPage>
        );
    }
};

export default Page;
